﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Cycle_While : MonoBehaviour
{
    //Задание 1
    [SerializeField]
    [Range (0, 100)]
    int numbers = 0;
    int i = 0;
    int sum = 0;

    [Space (10)]

    //Задание 2
    string Text;
    [SerializeField]
    [Range (1, 10)]
    int t;

    [Space(10)]

    //Задание 4
    string Text1;
    [SerializeField]
    [Range(1, 10)]
    int p;

    [Space (10)]

    //Задание 4
    [SerializeField]
    [Range(0, 10)]
    int n = 1;
    void Start()
    {
        Summary();

        HappyLittleTree();
        HappyLittleCristmassTree();

        Mark();

        n = UnityEngine.Random.Range(1000, 9999);
        //    n % 1;
    }

    private void HappyLittleTree()
    {
         for (int y = 0; y < t; y++)
         {
            Text += " 0";
            Debug.Log(Text);
        }
    }

    private void HappyLittleCristmassTree()
    {
        for (int y = 0; y < p; y++)
        {
            if (y % 2 == 1)
            {
                Text1 += " 0";
            }
            else
                Text1 += " 1";
            Debug.Log(Text1);
        }
    }

    private void Summary()
    {
        while (i < numbers)
        {
            i++;
            sum = sum + i;
        }
        Debug.Log(sum);
    }

    private static void Mark()
    {
        int v = UnityEngine.Random.Range(1, 100);
        if (v <= 50)
        {
            Debug.Log("За " + v + " баллов вы получили не удовлетворительно");
        }
        else if (v <= 70)
        {
            Debug.Log("За " + v + " баллов вы получили удовлетворительно");
        }
        else if (v < 100)
        {
            Debug.Log("За " + v + " баллов вы получили отлично");
        }
    }

    // Update is called once per frame
    void Update()
    {
    }
}
