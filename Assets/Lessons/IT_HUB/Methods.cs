﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Methods : MonoBehaviour
{

    //задание 1
    [SerializeField]
    int value1 = 0;
    [SerializeField]
    int value2 = 0;

    [Space(10)]

    //задание 2
    [SerializeField]
    [TextArea]
    string warning = "Warning";

    [Space(10)]

    //Задание 3
    [SerializeField]
    [Range(0, 100)]
    float div = 3;

    [Space(10)]

    //Задание 4
    [SerializeField]
    [Range(0, 100)]
    float median1 = 3;
    [SerializeField]
    [Range(0, 100)]
    float median2 = 3;

    [Space(10)]

    //Задание 5
    [SerializeField]
    [Range(0, 100)]
    float power1 = 3;
    [SerializeField]
    [Range(0, 100)]
    float power2 = 3;

    [Space(10)]

    //Задание 6
    [SerializeField]
    int del1 = 0;
    [SerializeField]
    int del2 = 0;
    float delRes = 0;
    void Start()
    {
        value1 = Add(value1, value2);
        value1 = Add(value1, value2);
        Debug.Log("Задание 1 - " + value1);

        PrintWarining( warning);

        div = DivideByThree(div);
        Debug.Log("Задание 3 - " + div);

        median1 = СalculateMedian(median1, median2);
        Debug.Log("Задание 4 - " + median1);

        power1 = СalculatePower(power1, power2);
        Debug.Log("Задание 5 - " + power1);

        delRes = RestOfDivision(del1, del2);
        Debug.Log("Задание 6 - " + delRes);
    }

    static int Add(int first, int second)
    {
        int result = first + second;

        return result;
    }

    void PrintWarining(string war)
    {
        Debug.Log("Задание 2 : " + "Зафиксировано предупреждение: " + war);
    }

    float DivideByThree(float a)
    {
        float result = a / 3;
        return result;
    }
    float СalculateMedian(float med1, float med2)
    {
        float result = (med1 + med2) / 2;
        return result;
    }

    float СalculatePower(float p1, float p2)
    {
        float result = (float)Math.Pow(p1, p2);
        return result;
    }

    float RestOfDivision(int o, int b)
    {
        float result =(float) o / b;
        return result;
    }
}
