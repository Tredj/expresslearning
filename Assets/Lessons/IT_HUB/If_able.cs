﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class If_able : MonoBehaviour
{
    //Задание 1
    [SerializeField]
    int level = 1;
    [SerializeField]
    int finishLevel = 3;

    [Space (10)]

    //Задание 2
    int exp = 0;
    int cLevel = 0;

    [Space(10)]

    //задание 3
    [SerializeField]
    string cName = "jeff";
    [SerializeField]
    bool check = false;

    [Space(10)]

    //Задание 4
    [SerializeField]
    [Range (-100, 100)]
    float dist = 0;
    void Start()
    {
        LevelChecker(level, finishLevel);
        NameChecker();
        DistanceChecker(dist);
    }

    private void Update()
    {
        CharacterLeveeler();
    }

    void LevelChecker(int a, int b)
    {
        if (level < finishLevel)
        {
            Debug.Log("Continue");
        }
        else
        {
            Debug.Log("Game Over");
        }
    }

    int CharacterLeveeler()
    {
        exp++;
        if (exp % 350 == 0)
        {
            cLevel++;
            Debug.Log("Достигнут уровень - " + cLevel);
        }
        return cLevel;
    }

    void NameChecker()
    {
        if (cName.Length < 4 && check == false)
        {
            Debug.Log("Error");
            return;
        }
        else if (cName.Length >= 4 && check == false)
        {
            Debug.Log("Вы должны согласиться с условиями использования сервиса");
        }

        else if (cName.Length < 4 && check == true)
        {

            Debug.Log("Ваше имя " + cName + " должно иметь минимум 4 буквы.");

        }
        else
        {
            Debug.Log("Вы авторезированы");
        }
    }

    float DistanceChecker(float a)
    {
        Debug.Log("Было: " + a);
        if (a < -10 || a > 10)
        {
            float result = a * (float)0.9;
            Debug.Log("Стало: " + result);
            return result;
        }
        else if (a > 0)
        {
            float result = a + 1000;
            Debug.Log("Стало: " + result);
            return result;
        }
        else if (a < 0)
        {
            float result = a - 1000;
            Debug.Log("Стало: " + result);
            return result;
        }
        return a;
    }
}
