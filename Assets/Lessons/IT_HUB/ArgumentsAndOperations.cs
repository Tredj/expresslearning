﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArgumentsAndOperations : MonoBehaviour
{
    //Задание 1
    [SerializeField]
    [TextArea]
    [Tooltip("Something like *Hello World*.")]
    string startText = "Hello World";

    [Space(10)]

    //Задание 2
    [SerializeField]
    string name = "Stefan";
    [SerializeField]
    string family = "Volkov";

    [Space(10)]

    //Задание3
    [SerializeField]
    string jimIdea = "really";
    [SerializeField]
    string samIdea = "cool";
    [SerializeField]
    string charlieIdea = "idea";

    [Space(10)]

    //Задание4
    [SerializeField]
    int value1 = 1;
    int value2 = 1;

    [Space(10)]

    //Задание5
    [SerializeField]
    int value3;
    int value4;

    void Start()
    {
        jimIdea = samIdea;
        jimIdea = samIdea + charlieIdea;

        value2 = (int)Math.Pow(value1, 2);
        value4 = (int)Math.Pow(value3, value3);

        Debug.Log(startText);
        Debug.Log("My full name is - " + name + " " + family);
        Debug.Log("So idea is " + jimIdea);
        Debug.Log("(Value1)Result is - " + value2);
        Debug.Log("(Value3)Result is - " + value4);
    }
}
